
import React from "react";
import { roundChange, createChangeItems, renderChangeRowes } from '../ReturnChange';

describe('roundChange()', () => {
  it('should truncate less than 0.05', () => {
    expect(roundChange(0.123)).toBe(0.12)
  })
  it('should round up if more than 0.05', () => {
    expect(roundChange(0.126)).toBe(0.13)
  })
  it('should handle string input', () => {
    expect(roundChange("0.126")).toBe(0.13)
  })
})


describe('createChangeItems()', () => {
  it('should create Change Row Item', () => {
    expect(createChangeItems(1.00)).toStrictEqual(["1 $Penney"]);
  })
})


describe('renderChangeRowes()', () => {
  it('should create a react fragment', () => {
    expect(renderChangeRowes(["1 $1 Dollar"])).toMatchInlineSnapshot(`
<React.Fragment>
  <div>
    1 $1 Dollar
    <br />
  </div>
</React.Fragment>
`);
  })
})