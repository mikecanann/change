import React from "react";

import { Form, InputGroup, FormControl, Row } from "react-bootstrap";
import NumberFormat from "react-number-format";
import configData from "../config/money.json";

export function currencyFormatter(value) {
  if (!Number(value)) {
    return "$0.00";
  }
  const formatConfig = {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 2,
    currencyDisplay: "symbol",
  };

  let amount = new Intl.NumberFormat("en-EN", formatConfig).format(value / 100);

  return `${amount}`;
}

export default function GetInputs(props) {
  function handleTotalChange(value) {
    props.setTotal(parseFloat(value) * 100);
  }

  function handleAmountChange(value) {
    props.setAmount(parseFloat(value) * 100);
  }

  return (
    <>
      <h2> Enter Money </h2>

      <div>
        <Form.Label htmlFor="basic-total-price">Total Price</Form.Label>
        <InputGroup className="mb-3">
          <NumberFormat
            id="basic-total-price"
            allowEmptyFormatting
            decimalScale={2}
            allowNegative={false}
            fixedDecimalScale
            value={Number(props.total) / 100}
            onValueChange={(e) => {
              if (e.value === "") {
                handleTotalChange(0.0);
              } else {
                handleTotalChange(parseFloat(e.value));
              }
            }}
            placeholder="0.00"
            format={currencyFormatter}
          />
        </InputGroup>
      </div>
      <br />
      <div>
        <Form.Label htmlFor="basic-total-price">Amount Given</Form.Label>
        <InputGroup className="mb-3">
          <NumberFormat
            allowEmptyFormatting
            decimalScale={2}
            allowNegative={false}
            fixedDecimalScale
            value={Number(props.amount) / 100}
            onValueChange={(e) => {
              if (e.value === "") {
                handleAmountChange(0.0);
              } else {
                handleAmountChange(parseFloat(e.value));
              }
            }}
            placeholder="0.00"
            format={currencyFormatter}
          />
        </InputGroup>
      </div>
    </>
  );
}
