import React, { useState } from "react";

import configData from "../config/money.json";

// handle floating point numbers (rounding to two decimal places)
export function roundChange(change) {
  return Number(Number(change).toFixed(2));
}

export function createChangeItems(changeDue) {
  let change = changeDue / 100;

  let values = [];

  for (let i = 0; i < configData.denominations.length; i++) {
    let denominationCount = Math.floor(
      change / configData.denominations[i].amount,
    );

    if (denominationCount > 1) {
      values.push(
        `${denominationCount} ${configData.moneySymbol}${configData.denominations[i].namePlural}`,
      );
    } else if (denominationCount === 1) {
      values.push(
        `${denominationCount} ${configData.moneySymbol}${configData.denominations[i].name}`,
      );
    }
    change %= configData.denominations[i].amount;

    change = roundChange(change);
  }

  return values;
}

export function renderChangeRowes(values) {
  return (
    <>
      {values.map((item, j) => (
        <div key={j}>
          {item}

          <br />
        </div>
      ))}
    </>
  );
}

export default function ReturnChange(props) {
  let changeDue = props.amount / 100 - props.total / 100;

  if (changeDue < 0) {
    changeDue = 0;
  }

  return (
    <>
      <h2> Changed Returned </h2>
      {renderChangeRowes(createChangeItems(changeDue))}
    </>
  );
}
