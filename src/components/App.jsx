import React, { useState } from "react";

import Header from "./Header";
import GetInputs from "./GetInputs";
import ReturnChange from "./ReturnChange";
import Footer from "./Footer";

import { Form, Col, Row } from "react-bootstrap";

import "bootstrap/dist/css/bootstrap.min.css";

export default function App(props) {
  const [totalCost, setTotalCost] = useState(0);
  const [amountProvided, setAmountProvided] = useState(0);

  return (
    <Form>
      <Row>
        <Col sm={4}></Col>
        <Col sm={4}>
          <Header />
          <GetInputs
            total={totalCost}
            amount={amountProvided}
            setTotal={setTotalCost}
            setAmount={setAmountProvided}
          />
          <ReturnChange total={totalCost} amount={amountProvided} />
          <Footer />
        </Col>
        <Col sm={4}></Col>
      </Row>
    </Form>
  );
}
